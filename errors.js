define([
	'require',
	'madjoh_modules/msgbox/msgbox',
	'madjoh_modules/session/session',
],
function (require, MsgBox, Session){
	var Errors = {
		onDeprecated : function(result){
			Session.close();
			MsgBox.show('deprecated_version');
		},
		onDisconnected : function(result){
			Session.close();
			MsgBox.show('sync_error');
		}
	};

	return Errors;
});